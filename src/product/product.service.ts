import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let product: Product[] = [
  { id: 1, name: 'user1', price: 50 },
  { id: 2, name: 'user2', price: 100 },
  { id: 3, name: 'user3', price: 150 },
];

let larstProductId = 4;

@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newPro: Product = {
      id: larstProductId++,
      ...createProductDto,
    };
    product.push(newPro);
    return newPro;
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return product[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...product[index],
      ...updateProductDto,
    };
    product[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = product[index];
    product.splice(index, 1);
    return deleteProduct;
  }
  resetPro() {
    product = [
      { id: 1, name: 'user1', price: 50 },
      { id: 2, name: 'user2', price: 100 },
      { id: 3, name: 'user3', price: 150 },
    ];
    larstProductId = 4;
    return 'RESET';
  }
}
