import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
